import './vendor'

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';
import { AppModule } from './app/app.module';

import { Observable } from 'rxjs/Observable'
import  'rxjs/add/operator/do'
const debuggerOn = true

Observable.prototype.debug = function (message: string) {
  return this.do (
    nextValue => {
      if (debuggerOn) {
        console.log(message, nextValue)
      }
    },
    error => {
      if (debuggerOn) {
        console.error(message, error)
      }
    },
    () =>  {
      if (debuggerOn) {
        console.log("Observer completed - ", message)
      }
    }

  )
}

declare module 'rxjs/Observable' {
  interface Observable<T> {
    debug: (...any) => Observable<T>
  }
}

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
