import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/let';

import { Store } from '@ngrx/store';
import { IAppState } from './store';

import { getPaginatedRestaurants, getRestaurants, getLoading, getCurrentPage, getPageTotal, getPages, getTerm, getLocation, getSortBy } from './store';

import { IRestaurant } from './models';

@Component({
  selector: 'restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.css']
})
export class RestaurantsComponent implements OnInit {

  loading$: Observable<boolean>;
  term$: Observable<string>;
  location$: Observable<string>;
  sortby$: Observable<string>
  restaurants$: Observable<IRestaurant[]>;
  paginatedRestaurants$: Observable<IRestaurant[]>;
  page$: Observable<number>;
  pageTotal$: Observable<number>;
  pages$: Observable<number[]>;
  
  constructor(private store: Store<IAppState>) { 
    this.paginatedRestaurants$ = store.let(getPaginatedRestaurants);
    this.restaurants$ = store.let(getRestaurants);
    this.loading$ = store.let(getLoading);
    this.page$ = store.let(getCurrentPage);
    this.pageTotal$ = store.let(getPageTotal);
    this.pages$ = store.let(getPages);
    this.term$ = store.let(getTerm);
    this.location$ = store.let(getLocation);
    this.sortby$ = store.let(getSortBy);
  }

  ngOnInit() {
  }

}
