import { Component, Input,  OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';

import { IRestaurant } from "../../models";

@Component({
  selector: 'restaurant-summary',
  templateUrl: './restaurant-summary.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class RestaurantSummaryComponent implements OnInit {

  @Input() restaurants: Array<IRestaurant>;

  constructor() { }

  ngOnInit() {
  }


}
