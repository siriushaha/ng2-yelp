import { Component, Input, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';

import { IAppState, SearchActions, RestaurantActions } from "../../store";
import { IRestaurant } from '../../models';

@Component({
  selector: 'search-restaurant',
  templateUrl: './search.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class SearchComponent implements OnInit {

  @Input() term: string;
  @Input() location: string;
  @Input() sortby: string;
  
  constructor(private store: Store<IAppState>,
              private searchActions: SearchActions,
              private restaurantActions: RestaurantActions) { }

  ngOnInit() {
  }
  
  setSortBy(sortby: string) {
    //console.log(sortby);
    this.store.dispatch(this.searchActions.setSortBy(sortby));
  }
  
  setTerm(term: string) {
    //console.log(term);
    this.store.dispatch(this.searchActions.setTerm(term));
  }

  setLocation(location: string) {
    //console.log(location);
    this.store.dispatch(this.searchActions.setLocation(location));
  }
  
  searchRestaurants(form) {
    //console.log(form.value);
    if (form.valid) {
      console.log(form.value)
      let { location, term, sortby } = form.value;
      this.store.dispatch(this.restaurantActions.searchRestaurants(location, term, sortby))
    }
  }


}
