
import  { ApolloClient, createNetworkInterface} from 'apollo-client';

const networkInterface = createNetworkInterface({
    uri: 'http://siriushaha.asuscomm.com:8082/graphql',
    /*
    opts: {
      credentials: 'include'
    }
    */
  });

/*

networkInterface.use([
  {
    applyMiddleware(req, next) {
      if (!req.options.headers) {
        req.options.headers = {};
      }
      req.options.headers['Access-Control-Allow-Origin'] = "*";
      req.options.headers['Access-Control-Allow-Methods'] = "POST, GET";
      next();
    }
  }
]);

*/
const graphqlClient: ApolloClient = new ApolloClient({
  networkInterface,
  connectToDevTools: true
});

export function provideClient(): ApolloClient {
  return graphqlClient;
}
