import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { ApolloModule } from 'apollo-angular';

//import { RatingModule } from "ngx-rating";
import { RatingModule } from '../shared/ratings/rating.module';

import { SmartadminModule } from "../shared/smartadmin.module";

import { RestaurantStoreModule } from './store';
import { restaurantsRouting } from './restaurants.routing';

import { RestaurantsComponent} from "./restaurants.component";
import { SearchComponent, RestaurantSummaryComponent, RestaurantComponent } from './components';

import { RestaurantService } from './services';
import { provideClient } from './graphql-client';
import { RestaurantEffects } from './store/effects/restaurant.effects';

@NgModule({
  imports: [
    CommonModule,
    restaurantsRouting,
    RatingModule,
    SmartadminModule,
    ApolloModule.forRoot(provideClient),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
    EffectsModule.run(RestaurantEffects),
    RestaurantStoreModule
  ],
  declarations: [RestaurantsComponent, SearchComponent, RestaurantSummaryComponent, RestaurantComponent],
  exports: [RestaurantsComponent],
  providers: []
})
export class RestaurantsModule { }
