import '@ngrx/core/add/operator/select';
import { Observable } from 'rxjs/Observable';
import { ActionReducer, Action } from '@ngrx/store';
import { IRestaurant } from '../../models';

import { RestaurantActionType } from '../actions/restaurant.actions'
//const paginated = require( 'paginated-redux')
import { IPaginatedState, totalPages, slicedList, paginate } from './pagination.reducers';

const _ = require('lodash');

export interface IRestaurantState extends IPaginatedState {
  loading?: boolean;
}

let initialRestaurantState: IRestaurantState = {
  list: [],
  loading: false
}

export const restaurants: ActionReducer<IRestaurantState> = (state: IRestaurantState = initialRestaurantState, action: Action) => {
  //console.log(`in sites reducer: ${action.type}`)
  switch (action.type) {
    case RestaurantActionType.SEARCH_RESTAURANTS_SUCCESS :
      return searchRestaurantsSuccess(state, action.payload);

    case RestaurantActionType.SEARCH_RESTAURANTS:
      return searchRestaurants(state);

    default:
      return state;
  }
}

function searchRestaurants(state: IRestaurantState): IRestaurantState {
  let newState: IRestaurantState = Object.assign({}, state, { loading: false });
  return newState;
}

function searchRestaurantsSuccess(state: IRestaurantState, restaurants: IRestaurant[]): IRestaurantState {
  const { page, per } = state;
  //const newList: IRestaurant[] = state.list.concat(restaurants);
  const newList: IRestaurant[] = [...restaurants];
  const total: number = totalPages(per, newList);
  const pages: Array<number> = _.range(page, total+1);
  let newState: IRestaurantState = Object.assign({}, state, {
    list: newList,
    pageList: slicedList(page, per, newList),
    total: total,
    pages: pages,
    loading: true
  });
  //console.log(newState)
  return newState;
}

export function getLoadingFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.loading);
}

export function getRestaurantsFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.list);
}

export function getPaginatedRestaurantsFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.pageList);
}

export function getCurrentPageFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.page);
}

export function getPageTotalFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.total);
}

export function getPagesFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.pages);
}

export function getCountPerPageFromRestaurantState(state$: Observable<IRestaurantState>) {
  return state$.select(s => s.per);
}

export const paginatedRestaurants = paginate(restaurants, {
  GOTO: RestaurantActionType.GOTO_RESTAURANT_PAGE,
  NEXT: RestaurantActionType.NEXT_RESTAURANT_PAGE,
  PREV: RestaurantActionType.PREV_RESTAURANT_PAGE
}, {
  defaultPage: 1,
  defaultPer: 10,
  defaultTotal: 0
})

