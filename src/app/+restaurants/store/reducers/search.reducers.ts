import '@ngrx/core/add/operator/select';
import { Observable } from 'rxjs/Observable';
import { ActionReducer, Action } from '@ngrx/store';

import { SearchActionType } from '../actions/search.actions'
//const paginated = require( 'paginated-redux')
import { IPaginatedState, totalPages, slicedList, paginate } from './pagination.reducers';

const _ = require('lodash');

export interface ISearchState {
  term: string | null;
  location: string | null;
  sortby: string;
}

let initialSearchState: ISearchState = {
  term: 'restaurants',
  location: null,
  sortby: 'best_match'
}

export const search: ActionReducer<ISearchState> = (state: ISearchState = initialSearchState, action: Action) => {
  //console.log(`in sites reducer: ${action.type}`)
  switch (action.type) {
    case SearchActionType.SET_TERM :
      return setTerm(state, action.payload);

    case SearchActionType.SET_LOCATION:
      return setLocation(state, action.payload);

    case SearchActionType.SET_SORT_BY:
      return setSortBy(state, action.payload);

    default:
      return state;
  }
}

function setTerm(state: ISearchState, term: string): ISearchState {
  let newState: ISearchState = Object.assign({}, state, { term });
  return newState;
}

function setLocation(state: ISearchState, location: string): ISearchState {
  let newState: ISearchState = Object.assign({}, state, { location });
  return newState;
}

function setSortBy(state: ISearchState, sortby: string): ISearchState {
  let newState: ISearchState = Object.assign({}, state, { sortby });
  return newState;
}

export function getTermFromSearchState(state$: Observable<ISearchState>) {
  return state$.select(s => s.term);
}

export function getLocationFromSearchState(state$: Observable<ISearchState>) {
  return state$.select(s => s.location);
}

export function getSortByFromSearchState(state$: Observable<ISearchState>) {
  return state$.select(s => s.sortby);
}

