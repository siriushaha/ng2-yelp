import { ActionReducer, Action } from '@ngrx/store'
import { PaginationActionType } from '../actions/pagination.actions'

// Return the total number of pages that can be made from `list`.
export const totalPages = (per = 10, list = []) => {
  const total = Math.ceil(list.length / per);
  return total ? total : 0;
};

// Return a slice of all `list` starting at `start` up to `per`
// (or the length of list; whichever comes first).
export const slicedList = (page = 1, per = 10, list = []) => {
  const start = (page - 1) * per;
  const end = per === 0 ? list.length : start + per;

  return end === list.length ?
    list.slice(start) :
    list.slice(start, end);
};

export interface IPaginatedState {
  list?: Array<any>;
  pageList?: Array<any>;
  page?: number;
  total?: number;
  per?: number;
  pages?: Array<number>;
};
// params:
// 1. the reducer being augmented
// 2. definitions of action types
// 3. options
export const paginate = (
  reducer,
  {
    GOTO=PaginationActionType.GOTO,
    NEXT=PaginationActionType.NEXT,
    PREV=PaginationActionType.PREV
  } = {},
  {
    defaultPage= 1,
    defaultPer= 10,
    defaultTotal= 0
  } = {}
) => {
  // NOTE: the reducer's array is named "list" at this point.
  // TODO: Is there a way to define the name of this property outside this module?
  // NOTE: cacheList is a temporary cached array of sorted + filtered elements
  // from the total list so that it doesn't need to be re-calculated each time
  // the pagedList function is called.
  const initialPaginatedState: IPaginatedState = {
    list: [],
    pageList: [],
    page: defaultPage,
    total: defaultTotal,
    per: defaultPer
  };

  return (state: IPaginatedState = initialPaginatedState, action: Action) => {
    const { list, page, per, total } = state;

    // NOTE: I'm using blocks (i.e., statments wrapped in {}) for a few
    // conditions so that I can reuse the same variable const in different
    // blocks without causing a duplicate declaration conflicts.

    //console.log(`in paginate reducer: ${action.type}`)
    //console.log(NEXT)
    //console.log(PREV)

    let newState

    switch (action.type) {

      // Go to a specific page. Can be used to initialize the list into a certain
      // page state.
      case GOTO:
        if (page == action.payload)
          return state;
        newState = Object.assign({}, state, {
          page: action.payload,
          pageList: slicedList(action.payload, per, list)
        });
        return reducer(newState, action);

      // If the the action is fired whilst at the end of the list, swing around
      // back to the beginning.
      case NEXT:
        if (page >= total)
          return state;
        let nextPage = page + 1;
        //if (nextPage > state.list.length - 1) nextPage = 0;let
        newState =  Object.assign({}, state, {
          page: nextPage,
          pageList: slicedList(nextPage, per, list)
        });
        return reducer(newState, action);

      // If the action is fired whilst already at the beginning of the list,
      // swing around to the end of the list (this behaviour can be handled
      // differently through the UI if this is not the desired behaviour, for
      // example, by simply not presenting the user with the "prev" button at
      // all if already on the first page so it is not possible to wrap around).
      case PREV:
        if (page <= 1)
          return state;
        let prevPage = page - 1;
        //if (prevPage < 0) prevPage = state.list.length - 1;
        newState = Object.assign({}, state, {
          page: prevPage,
          pageList: slicedList(prevPage, per, list)
        });
        return reducer(newState, action);

      // Setup the default list and cache and calculate the total.
      default:
        //console.log(`in paginate default`)
        return reducer(state, action);
    }

  };
};
