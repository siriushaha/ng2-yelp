import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
//import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { RestaurantActions } from './actions/restaurant.actions';
import { SearchActions } from './actions/search.actions';

// reducers
import { storeReducers } from './store';

import { RestaurantService } from '../services';

export * from './actions/restaurant.actions';
export * from './actions/search.actions';
export * from './store';

const ACTIONS = [
  RestaurantActions,
  SearchActions
];

const SERVICES = [
  RestaurantService
];

@NgModule({
  imports: [
    StoreModule.provideStore(storeReducers),
  ],
  declarations: [],
  exports: [],
  providers: [ SERVICES, ACTIONS ]
})
export class RestaurantStoreModule {};
