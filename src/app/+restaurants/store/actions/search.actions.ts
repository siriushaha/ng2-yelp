import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

export const SearchActionType = {
	SET_TERM:     'SET_TERM',
	SET_LOCATION: 'SET_LOCATION',
	SET_SORT_BY:  'SET_SORT_BY'
}

@Injectable()
export class SearchActions {

  constructor() {}

  setTerm(term: string): Action {
    return {
      type: SearchActionType.SET_TERM,
      payload: term
    }
  }

  setLocation(location: string): Action {
    return {
      type: SearchActionType.SET_LOCATION,
      payload: location
    }
  }
  
  setSortBy(sortby: string): Action {
    return {
      type: SearchActionType.SET_SORT_BY,
      payload: sortby
    }
  }

}
