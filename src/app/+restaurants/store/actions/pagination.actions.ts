export const PaginationActionType = {
	GOTO: 'GOTO',
	NEXT: 'NEXT',
	PREV: 'PREV'
}

