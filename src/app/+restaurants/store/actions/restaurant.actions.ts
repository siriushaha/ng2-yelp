import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { IRestaurant} from '../../models';
//import { IndexedDbService } from '../../services';

export const RestaurantActionType = {
	SEARCH_RESTAURANTS:         'SEARCH_RESTAURANTS',
	SEARCH_RESTAURANTS_SUCCESS: 'SEARCH_RESTAURANTS_SUCCESS',
	SEARCH_RESTAURANTS_FAILED:  'SEARCH_RESTAURANTS_FAILED',
	GOTO_RESTAURANT_PAGE:       'GOTO_RESTAURANT_PAGE',
	NEXT_RESTAURANT_PAGE:       'NEXT_RESTAURANT_PAGE',
	PREV_RESTAURANT_PAGE:       'PREV_RESTAURANT_PAGE'
}

@Injectable()
export class RestaurantActions {

  constructor() {}

  searchRestaurants(location: string, term: string, sortby: string): Action {
    return {
      type: RestaurantActionType.SEARCH_RESTAURANTS,
      payload: { location, term, sortby }
    }
  }

  searchRestaurantsSuccess(searchResult: any): Action {
    console.log(searchResult)
    let restaurants: IRestaurant[] = searchResult.restaurants.businesses;
    console.log(restaurants);
    return {
      type: RestaurantActionType.SEARCH_RESTAURANTS_SUCCESS,
      payload: restaurants
    }
  }

  searchRestaurantsFailed(): Action {
    return {
      type: RestaurantActionType.SEARCH_RESTAURANTS_FAILED
    }
  }

  gotoRestaurantPage(page: number): Action {
    return {
      type: RestaurantActionType.GOTO_RESTAURANT_PAGE,
      payload: page
    }
  }

  gotoNextRestaurantPage(): Action {
    return {
      type: RestaurantActionType.NEXT_RESTAURANT_PAGE
    }
  }

  gotoPrevRestaurantPage(): Action {
    return {
      type: RestaurantActionType.PREV_RESTAURANT_PAGE
    }
  }

/*
  static SORT_SITES = 'SORT_SITES'
  sortSites(by: string): Action {
    return {
      type: SiteActions.SORT_SITES,
      payload: by
    }
  }

  static FILTER_SITES = 'FILTER_SITES'
  filterSites(filter: string): Action {
    return {
      type: SiteActions.FILTER_SITES,
      payload: filter
    }
  }
*/

}
