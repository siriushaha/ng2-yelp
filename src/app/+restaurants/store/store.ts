import { Observable } from 'rxjs/Observable';
import { compose } from "@ngrx/core/compose";
import '@ngrx/core/add/operator/select';
//import { localStorageSync } from './reducers/ngrx-store-localstorage';

import { ActionReducer, combineReducers } from '@ngrx/store';

import { paginatedRestaurants , IRestaurantState, getLoadingFromRestaurantState, getRestaurantsFromRestaurantState,
  getPaginatedRestaurantsFromRestaurantState, getCurrentPageFromRestaurantState, getPageTotalFromRestaurantState,
  getCountPerPageFromRestaurantState, getPagesFromRestaurantState } from './reducers/restaurant.reducers';

import { search, ISearchState, getTermFromSearchState, getLocationFromSearchState, getSortByFromSearchState } from './reducers/search.reducers';
/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface IAppState {
  search: ISearchState;
  paginatedRestaurants: IRestaurantState;
}

export function getRestaurantState(state$: Observable<IAppState>) {
  return state$.select(s => s.paginatedRestaurants);
}

export const getLoading = compose(getLoadingFromRestaurantState,getRestaurantState);
export const getPaginatedRestaurants = compose(getPaginatedRestaurantsFromRestaurantState,getRestaurantState);
export const getPageTotal = compose(getPageTotalFromRestaurantState,getRestaurantState);
export const getCurrentPage = compose(getCurrentPageFromRestaurantState,getRestaurantState);
export const getCountPerPage = compose(getCountPerPageFromRestaurantState,getRestaurantState);
export const getPages = compose(getPagesFromRestaurantState,getRestaurantState);
export const getRestaurants = compose(getRestaurantsFromRestaurantState,getRestaurantState);

export function getSearchState(state$: Observable<IAppState>) {
  return state$.select(s => s.search);
}

export const getTerm = compose(getTermFromSearchState,getSearchState);
export const getLocation = compose(getLocationFromSearchState,getSearchState);
export const getSortBy = compose(getSortByFromSearchState,getSearchState);

/*
 const composeStore = compose(
 localStorageSync([ 'paginatedRestaurants', 'search'], false),
 combineReducers
 )({  paginatedRestaurants, search });
*/

const reducers = { paginatedRestaurants, search };

const composeStore: ActionReducer<IAppState> = combineReducers(reducers);

export function storeReducers(state: any, action: any) {
  return composeStore(state,action);
}