import { Injectable } from '@angular/core'
import { Action } from "@ngrx/store";
import { Effect, Actions, toPayload } from '@ngrx/effects'
import { RestaurantActions, RestaurantActionType } from '../actions/restaurant.actions'
import { RestaurantService } from '../../services'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';

@Injectable()
export class RestaurantEffects {
	
  constructor (
    private action$: Actions,
    private restaurantActions: RestaurantActions,
    private restaurantService: RestaurantService
  ) {}

  @Effect()
  searchRestaurants$: Observable<Action> = this.action$
    .ofType(RestaurantActionType.SEARCH_RESTAURANTS)
    .map(toPayload)
    .debug('searchRestaurants')
    .switchMap((payload) => {
      return this.restaurantService.searchRestaurants(payload.location, payload.term, payload.sortby)
        					.map(restaurants$ => this.restaurantActions.searchRestaurantsSuccess(restaurants$));
        					
    });
}