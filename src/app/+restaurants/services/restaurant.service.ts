import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Apollo, ApolloQueryObservable } from 'apollo-angular';

import gql from 'graphql-tag';

const SearchRestaurants = gql`
query yelp($term: String!, $location: String!, $sortby: String!, $limit: Int) {
	restaurants: searchForBusiness(term: $term, location: $location, sortby: $sortby, limit: $limit) {
		businesses {
  		...businessInfo
		}
	}
}
fragment businessInfo on Business {
  id,
  name,
  image_url,
  url,
  price,
  phone,
  rating,
  review_count,
  distance,
  categories {
    title
  },
  location {
    display_address
  }
}

`;


@Injectable()
export class RestaurantService {

  constructor(private apollo: Apollo) {}

  searchRestaurants(location: string, term: string, sortby: string): Observable<any> {
    let query = { query: SearchRestaurants,
    							variables: { location, term, sortby, limit: 10 }
    						};
		console.log(query)
    return this.apollo.watchQuery(query)
            	.map(({data}) => data );
  }

}
